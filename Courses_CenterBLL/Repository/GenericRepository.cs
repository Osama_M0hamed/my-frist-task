﻿using Courses_CenterBLL.Interfaces;
using Courses_CenterDAL.Contexts;
using Courses_CenterDAL.Model;
using Microsoft.EntityFrameworkCore;

namespace Courses_CenterBLL.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly CouresesCenterDbContext dbContext;

        public GenericRepository(CouresesCenterDbContext dbContext )
        {
            this.dbContext = dbContext;
        }
        public async Task Add(T item)
         => await dbContext.Set<T>().AddAsync(item);
        public void Delete(T item)
         => dbContext.Set<T>().Remove(item);
        public async Task<T> Get(int id)
       => await dbContext.FindAsync<T>(id);
        public async Task<IEnumerable<T>> GetAll()
       => await dbContext.Set<T>().ToListAsync();    
        public void Update(T item)
           => dbContext.Set<T>().Update(item);
    }
    
    
}
