﻿using Courses_CenterBLL.Interfaces;
using Courses_CenterDAL.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Courses_CenterBLL.Repository
{
    public class Unitofwork : IUnitOfWork
    {
        private readonly CouresesCenterDbContext dbContext;
        public IStudentRepository StudentRepository { get; set; }
        public ITeacherRepository TeacherRepository { get; set; }
        public ICoursesRepository CourseseRepository { get ; set; }

        public Unitofwork(CouresesCenterDbContext dbContext)
        {
            CourseseRepository = new CourseseRepository(dbContext);
            StudentRepository = new StudentRepository(dbContext);  
            TeacherRepository = new TeacherRepository(dbContext);

            this.dbContext = dbContext;
        }


        public async Task<int> Complete()
         => await dbContext.SaveChangesAsync();

        public void Dispose()
         =>dbContext.Dispose();

    }
}
