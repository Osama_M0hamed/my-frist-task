﻿using Courses_CenterBLL.Interfaces;
using Courses_CenterDAL.Contexts;
using Courses_CenterDAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Courses_CenterBLL.Repository
{
    public class TeacherRepository : GenericRepository<Teacher>, ITeacherRepository
    {
        public TeacherRepository(CouresesCenterDbContext dbContext) : base(dbContext)
        {
        }
    }
}
