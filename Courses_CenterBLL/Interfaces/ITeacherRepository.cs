﻿using Courses_CenterDAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Courses_CenterBLL.Interfaces
{
    public interface ITeacherRepository :IGenericRepository<Teacher>
    {
    }
}
