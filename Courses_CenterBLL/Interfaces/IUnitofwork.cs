﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Courses_CenterBLL.Interfaces
{
    public interface IUnitOfWork :IDisposable
    {
        ICoursesRepository CourseseRepository { get; set; }
        IStudentRepository StudentRepository { get; set; }
        ITeacherRepository TeacherRepository { get; set; }
        Task<int> Complete();
        public void Dispose();




    }
}
