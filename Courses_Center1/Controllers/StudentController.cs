﻿using AutoMapper;
using Courses_CenterBLL.Interfaces;
using Courses_CenterDAL.Model;
using Courses_CenterPL.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Courses_CenterPL.Controllers
{
    public class StudentController : Controller
    {
        private readonly IUnitOfWork Unitofwork;
        private readonly IMapper mapper;

        public StudentController(IUnitOfWork Unitofwork, IMapper mapper)
        {
            this.Unitofwork = Unitofwork;
            this.mapper = mapper;
        }
        public async Task< IActionResult> Index()
        {
            var student = await Unitofwork.StudentRepository.GetAll();
            var StudentVm = mapper.Map<IEnumerable<Students>,IEnumerable<StudentViewModel>>(student);
            return View(StudentVm);
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(StudentViewModel studentvm)
        {
            if (ModelState.IsValid) // server side Validation
            {
                var MappedDR = mapper.Map<StudentViewModel, Students>(studentvm);
                await Unitofwork.StudentRepository.Add(MappedDR);
                {
                    TempData["Message"] = "Department is Created";
                }
                return RedirectToAction(nameof(Index));
            }
            return View();
        }
       


    }
    }

