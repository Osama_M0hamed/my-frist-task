﻿using AutoMapper;
using Courses_CenterDAL.Model;
using Courses_CenterPL.ViewModels;

namespace Courses_CenterPL.MapperProfile
{
    public class MapingProfile:Profile
    {
        public MapingProfile()
        {
            CreateMap<CoursesViewModel, Courses>().ReverseMap();
            CreateMap<StudentCoursesViewModel, StudentCourses>().ReverseMap();
            CreateMap<StudentViewModel, Students>().ReverseMap();
            CreateMap<TeacherViewModel, Teacher>().ReverseMap();

        }
    }
}
