﻿using Courses_CenterDAL.Model;

namespace Courses_CenterPL.ViewModels
{
    public class StudentCoursesViewModel
    {
        public int id { get; set; }
        public int StudentsId { get; set; }
        public int CoursesId { get; set; }
        public int TeacherId { get; set; }
        public Courses Courses { get; set; }
        public Students Students { get; set; }
        public Teacher Teacher { get; set; }
    }
}
