﻿using Courses_CenterDAL.Model;

namespace Courses_CenterPL.ViewModels
{
    public class TeacherViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int phone { get; set; }
        public string Address { get; set; }
        public Courses Courses { get; set; }

    }
}
