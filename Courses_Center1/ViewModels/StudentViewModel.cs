﻿using Courses_CenterDAL.Model;
using System.ComponentModel.DataAnnotations;

namespace Courses_CenterPL.ViewModels
{
    public class StudentViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Name Is Required")]
        [MaxLength(50, ErrorMessage = "Max Length is 50 Chars")]
        [MinLength(5, ErrorMessage = "Min Length is 50 Chars")]
        public string Name { get; set; }
       
        [Required(ErrorMessage = "Address Is Required")]

        public string Address { get; set; }
        [Required(ErrorMessage = "Phone Is Required")]

        public int Phone { get; set; }
        public ICollection<StudentCourses> StudentCourses { get; set; } = new HashSet<StudentCourses>();

    }
}
