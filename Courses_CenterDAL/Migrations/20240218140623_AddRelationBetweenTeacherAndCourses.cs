﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Courses_CenterDAL.Migrations
{
    public partial class AddRelationBetweenTeacherAndCourses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "StudentCourses",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_StudentCourses_TeacherId",
                table: "StudentCourses",
                column: "TeacherId");

            migrationBuilder.AddForeignKey(
                name: "FK_StudentCourses_Teacher_TeacherId",
                table: "StudentCourses",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StudentCourses_Teacher_TeacherId",
                table: "StudentCourses");

            migrationBuilder.DropIndex(
                name: "IX_StudentCourses_TeacherId",
                table: "StudentCourses");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "StudentCourses");
        }
    }
}
