﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Courses_CenterDAL.Migrations
{
    public partial class EditStudetName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FName",
                table: "Students");

            migrationBuilder.RenameColumn(
                name: "LName",
                table: "Students",
                newName: "Name");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Students",
                newName: "LName");

            migrationBuilder.AddColumn<string>(
                name: "FName",
                table: "Students",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
