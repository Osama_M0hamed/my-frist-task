﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Courses_CenterDAL.Model
{
    public class Courses
    {
        public  int Id { get; set; }    
        public string Name { get; set; }    
        public ICollection<Teacher> Teachers { get; set; } = new HashSet<Teacher>();
        public ICollection<StudentCourses> StudentCourses { get; set; } = new HashSet<StudentCourses>();


    }
}
