﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Courses_CenterDAL.Model
{
    public class StudentCourses
    {
        public int id { get; set; } 
        public int StudentsId { get; set; }
        public int CoursesId { get; set; }  
        public int TeacherId { get; set; }    
        public Courses Courses { get; set; }
        public Students Students { get; set; }  
        public Teacher Teacher { get; set; }

    }
}
