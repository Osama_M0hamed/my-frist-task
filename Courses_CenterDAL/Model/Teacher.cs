﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Courses_CenterDAL.Model
{
    public class Teacher
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int phone { get; set; }
        public string Address { get; set; } 
        public Courses Courses { get; set; }  
       
    }
}
