﻿using Courses_CenterDAL.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Courses_CenterDAL.Contexts
{
    public class CouresesCenterDbContext:DbContext
    {
        public CouresesCenterDbContext(DbContextOptions<CouresesCenterDbContext> options ):base(options)
        {

        }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("Server=.;Database=CenterCoures;Trusted_Connection=true");
        //}

        public DbSet<Students> Students { get; set; }
        public DbSet<Teacher> Teacher { get; set; }
        public DbSet<Courses> Courses { get; set; }



    }
}
